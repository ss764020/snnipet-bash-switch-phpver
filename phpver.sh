#!/bin/bash
echo ""
echo ""
echo "===================================="
echo "    - PHP VERSION SWITCHER -"
echo "===================================="

# 現在選択中のPHPバージョンを出力
echo ""
echo "====== Now using ====="
PNOW=`php -v | grep -E "^PHP [0-9]+" | sed -e "s/^PHP \([0-9]\+\.[0-9]\+\).\+$/\1/g"`
echo "PHP $PNOW"

# 選択可能なPHPバージョンを一覧出力
echo ""
echo "====== You can use ====="
PLIST=(`update-alternatives --list php | grep -E "php[0-9]+" | sed -e "s/^\/usr\/bin\/php\([0-9]\+\.[0-9]\+\)$/\1/g"`)
i=0
for PV in ${PLIST[@]}; do
  echo "[$i] => PHP $PV"
  let i++
done
let i--
echo "========================"

# PHPのバージョンを選択
echo ""
read -p "Plz select PHP version(0-$i):" SELECTED

# 入力値チェック
if [[ ! "$SELECTED" =~ ^[0-9]+$ ]]; then
  echo "- inputed value is not number "
  exit 1
  echo ""
fi
if [[ "$SELECTED" -gt $i ]]; then
  echo "- inputed number is not exist -"
  exit 1
  echo ""
fi
PNEW=${PLIST[$SELECTED]}
if [[ "$PNEW" = "$PNOW" ]]; then
  echo "- no changes -"
  exit 1
  echo ""
fi

# 情報出力
echo ""
echo ""
echo "===================================="
echo "    Switching PHP $PNOW >> $PNEW"
echo "===================================="

# 現在選択中のPHPを無効化
echo ""
echo "====== Disabling PHP module ====="
sudo a2dismod php$PNOW

# 新規選択PHPを有効化
echo ""
echo "====== Enabling _PHP module ====="
sudo a2enmod php$PNEW

# Apache再起動
echo ""
echo "====== Restarting APACHE ====="
sudo service apache2 restart

# CLI上のPHPバージョン を切り替え
echo ""
echo "====== Switching PHP-CLI ====="
sudo update-alternatives --set php /usr/bin/php$PNEW

# 終了
echo ""
echo "- fin -"
echo ""